'use strict';

var gulp = require('gulp');
var config = require('./../config');

gulp.task('copy:server', ['clean'], function () {
	return gulp.src(config.paths.server, {
			base: '.'
		})
		.pipe(gulp.dest(config.paths.build));
});
