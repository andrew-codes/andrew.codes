'use strict';

var gulp = require('gulp');
var stylus = require('gulp-stylus');
var notifyError = require('./../utils/notifyError');
var autoprefix = require('gulp-autoprefixer');
var path = require('path');
var config = require('./../config.js');

gulp.task('stylus', [], function () {
	return gulp.src('./../../styles/index.styl')
		.pipe(stylus())
		.on('error', notifyError)
		.pipe(autoprefix())
		.on('error', notifyError)
		.pipe(gulp.dest(path.join(config.paths.public, 'css')));
});
