'use strict';

var gulp = require('gulp');
var config = require('./../config');
var path = require('path');
var cachebust = require('gulp-cachebust')();

gulp.task('bust:collect', ['stylus', 'webpack', 'copy'], function () {
	var src = [].concat(config.paths.public + '**/*');
	return gulp.src(src, {
			cwd: config.paths.build,
			base: path.join(config.paths.build, config.paths.public)
		})
		.pipe(cachebust.resources());
});
