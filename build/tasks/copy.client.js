'use strict';

var gulp = require('gulp');
var config = require('./../config');
var filter = require('gulp-filter');
var minifyCss = require('gulp-minify-css');

gulp.task('copy:public', ['clean', 'stylus'], function () {
	var src = [config.paths.public + '**/*', '!**/*.map'];
	var filterCSS = filter('**/*.css');

	return gulp.src(src, {
			base: '.'
		})
		.pipe(filterCSS)
		.pipe(minifyCss({
			keepBreaks: true
		}))
		.pipe(filterCSS.restore())
		.pipe(gulp.dest(config.paths.build));
});
