'use strict';

var gulp = require('gulp');
var removeDirAsync = require('./../utils/removeDirAsync');
var config = require('./../config');

gulp.task('clean', function (callback) {
	removeDirAsync(config.paths.build, callback);
});
