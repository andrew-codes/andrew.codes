'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var cachebust = require('gulp-cachebust')();
var config = require('./../config');

gulp.task('bust:replace', ['bust:collect'], function () {
	gutil.log('[bust:replace]', 'Busting ' + Object.keys(cachebust.mappings).length + ' asset(s)...');
	return gulp.src(config.paths.server, {
			cwd: config.paths.build,
			base: config.paths.build
		})
		.pipe(cachebust.references())
		.pipe(gulp.dest(config.paths.build));
});
