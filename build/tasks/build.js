'use strict';

var gulp = require('gulp');
var notifier = require('node-notifier');
var pkg = require('./../../package.json');
var config = require('./../config');
var gutil = require('gulp-util');

gulp.task('build', ['clean', 'stylus', 'webpack', 'copy', 'bust'], function () {
	notifier.notify({
		icon: null,
		contentImage: __dirname + '/public/images/favicon.png',
		title: pkg.name,
		sound: 'Glass',
		message: 'Build: done.',
		open: 'file://' + __dirname + '/' + config.paths.build
	});
	gutil.log('[build] Run `./scripts/prod` to test the built app.');
});
