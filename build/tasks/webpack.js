'use strict';

var gulp = require('gulp');
var webpack = require('webpack');
var gutil = require('gulp-util');
var notifyError = require('../utils/notifyError');
var webpackBuild = require('./../webpack.config');

gulp.task('webpack', ['clean'], function (callback) {
	webpack(webpackBuild, function (err, stats) {
		if (err) {
			return notifyError(err);
		}
		gutil.log('[webpack]', stats.toString({
			colors: true,
			hash: false,
			timings: false,
			assets: true,
			chunks: false,
			chunkModules: false,
			modules: false,
			children: true
		}));
		callback();
	});
});
