'use strict';

module.exports = {
	paths: {
		build: 'dist/',
		public: 'public/',
		style: 'style/main.styl',
		server: ['package.json', 'server.js', '*.jsx', 'cachebuster.js', 'components/**/*.jsx']
	}
};
