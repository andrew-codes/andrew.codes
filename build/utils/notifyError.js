'use strict';

var gutil = require('gulp-util');
var notifier = require('node-notifier');
var pkg = require('./../../package.json');

module.exports = function notifyError(err) {
	if (!err) {
		return;
	}
	gutil.log(err.message);
	gutil.beep();
	notifier.notify({
		title: 'Building ' + pkg.name,
		message: err.message
	});
};
