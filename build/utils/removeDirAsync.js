'use strict';

var fs = require('fs');
var _ = require('underscore');
var path = require('path');

function removeDirAsync(directoryPath, callback) {
	if (!fs.existsSync(directoryPath)){
		callback();
		return;
	}
	fs.readdir(directoryPath, function (error, files) {
		if (error) {
			callback(error, []);
			return;
		}
		var wait = files.length;
		var count = 0;

		function directoryDone(error) {
			if (error) {
				callback(error, []);
				return;
			}
			count++;
			if (count >= wait || error) {
				fs.rmdir(directoryPath, callback);
			}
		}

		if (!wait) {
			directoryDone(directoryPath, callback);
			return;
		}

		// Remove one or more trailing slash to keep from doubling up
		directoryPath = directoryPath.replace(/\/+$/, '');
		_.each(files, function (file) {
			var curPath = path.join(directoryPath, file);
			fs.lstat(curPath, function (err, stats) {
				if (err) {
					callback(err, []);
					return;
				}
				if (stats.isDirectory()) {
					removeDirAsync(curPath, directoryDone);
				} else {
					fs.unlink(curPath, directoryDone);
				}
			});
		});
	});
}

module.exports = removeDirAsync;
