'use strict';

var debug = require('debug')('server');
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var webpackConfig = require('./webpack.config.dev');

module.exports = function (port) {
	var webpackPort = 9001;
	// Run the webpack dev server
	new WebpackDevServer(webpack(webpackConfig), {
			publicPath: webpackConfig.output.publicPath,
			contentBase: 'http://localhost:' + port,
			noInfo: true,
			hot: true,
			headers: {
				'Access-Control-Allow-Origin': '*'
			}
		})
		.listen(webpackPort, 'localhost', function (err) {
			if (err) {
				console.log(err);
			} else {
				debug('Webpack server listening on port ' + webpackPort);
			}
		});

	return webpackPort;
};
