'use strict';

var React = require('react');
var Router = require('react-router');
var DocumentTitle = require('react-document-title');

var RouteHandler = Router.RouteHandler;

var title = 'Software Craftsmanship: A Blog by Andrew Smith';

var App = React.createClass({
	getDefaultProps: function () {
		return {  };
	},
	render: function () {
		return (
			<DocumentTitle title={ title }>
				<div>
					<header>
							<h1>{ title }</h1>
					</header>
					<main>
						<RouteHandler />
					</main>
					<footer>
					</footer>
				</div>
			</DocumentTitle>
		);
	}
});

module.exports = App;
