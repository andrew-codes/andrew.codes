'use strict';

var React = require('react');

var Html = React.createClass({
	getDefaultProps: function() {
		return {
			title: 'Blog Title'
		};
	},
	render: function() {
		return (
				<html>
				<head>
					<title>{ this.props.title }</title>
					<link rel="icon" type="image/png" href="/images/favicon.png" />
					<link rel="stylesheet" href="/css/main.css" />
					<script src="/js/lib.js"></script>
					<script src="/js/main.js"></script>
				</head>
				<body dangerouslySetInnerHTML={{__html: this.props.markup}}></body>
				</html>
		);
	}
});

module.exports = Html;
