'use strict';

var React = require('react');

var NotFound = React.createClass({
	render: function () {
		return <p>This is not the page you are looking for</p>;
	}
});

module.exports = NotFound;
