'use strict';

var webpack = require('webpack');

module.exports = {
	// Entry point for static analyzer:
	entry: [
		'webpack-dev-server/client?http://localhost:9001',
		'webpack/hot/dev-server',
		'./client.jsx',
		'./styles/index.styl'
	],

	output: {
		// Where to put build results when doing production builds:
		// (Server doesn't write to the disk, but this is required.)
		path: __dirname,

		// Filename to use in HTML
		filename: 'main.js',

		// Path to use in HTML
		publicPath: 'http://localhost:9001/js/'
	},

	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.optimize.CommonsChunkPlugin('lib', 'lib.js')
	],

	resolve: {
		extensions: ['', '.js', '.jsx']
	},

	module: {
		loaders: [{
			test: /\.(jsx|js)$/,
			loaders: ['react-hot', 'jsx-loader?harmony=true']
		}, {
			test: /\.(styl)$/,
			loaders: ['style-loader', 'css-loader', 'autoprefixer-loader', 'stylus-loader']
		}, {
			test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
			loader: 'url-loader?limit=10000&minetype=application/font-woff'
		}, {
			test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
			loader: 'url?limit=10000&mimetype=application/font-woff2'
		}, {
			test: /\.(ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
			loader: 'file-loader'
		}, {
			test: /.*\.(gif|jpg)$/,
			loader: ['file', 'image'],
			query: {
				progressive: true,
				interlaced: true,
				optimizationLevel: 7
			}
		}, {
			test: /\.png$/,
			loaders: ['url-loader', 'image'],
			query: {
				mimetype: 'image/png',
				progressive: true,
				interlaced: true,
				optimizationLevel: 7
			}
		}]
	},
	devtool: '#inline-source-map',
	externals: {}
};
