/* global document */
'use strict';

var React = require('react');
var Router = require('react-router');
var routes = require('./routes.jsx');

document.addEventListener('DOMContentLoaded', function() {
		Router.run(routes, Router.HistoryLocation, function (Handler) {
			React.render(<Handler />, document.body);
		});
});
